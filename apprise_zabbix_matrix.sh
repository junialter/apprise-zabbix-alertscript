#!/usr/bin/env bash

apprisepath=/usr/local/bin/apprise
matrixhostname=your.matrixserver.local
matrixsenderuser=matrixusername
matrixsenderpassword=matrixpassword
matrixroomid=!yourroomid


matrixroomid=$1
subject=$2
body=$3

$apprisepath -vv -t "${subject}" -b "${body}" 'matrixs://$matrixsenderuser:$matrixsenderpassword@$matrixhostname/$matrixroomid?format=markdown
